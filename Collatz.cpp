// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

const int64_t MAX_CACHE_SIZE = 500000;
int64_t MCL_Cache[MAX_CACHE_SIZE] = {0, 1, 2};

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}


// ------------
// compute_cycle_length
// ------------

int compute_cycle_length(int64_t x)
{
    int CL = 0;

    do
    {
        assert(x>0);
        if (x < MAX_CACHE_SIZE && MCL_Cache[x])
        {
            CL += MCL_Cache[x];
            break;
        }
        else
        {
            if (!(x%2))
            {
                x /= 2;
                ++CL;
            }
            else
            {
                x = ((x * 3) + 1)/2;
                CL += 2;
            }
        }
    } while (x != 1);
    return CL;
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int64_t high, low;
    int CCL, MCL = 0;

    try
    {
        if(p.first <= 0 || p.second <= 0)
            throw 1;
    }
    catch(int e)
    {
        return make_tuple(p.first, p.second, -1);
    }

    // Check input ordering
    if (p.first < p.second)
    {
        high = p.second;
        low = p.first;
    }
    else
    {
        high = p.first;
        low = p.second;
    }

// Apply Domain Reduction Optimization
    if(low < ((high / 2) + 1))
    {
        low = (high / 2) + 1;
    }

// Compute Max Cycle Length for [Low, High]
    for (int64_t x = low; x <= high; ++x)
    {
        CCL = compute_cycle_length(x);
        if (x < MAX_CACHE_SIZE)
            MCL_Cache[x] = CCL;
        if (CCL > MCL)
        {
            MCL = CCL;
        }
    }
    return make_tuple(p.first, p.second, MCL);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
