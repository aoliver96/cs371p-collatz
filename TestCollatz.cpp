// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1000)), make_tuple(1, 1000, 179));
}

TEST(CollatzFixture, eval6)
{
    ASSERT_EQ(collatz_eval(make_pair(3, 3)), make_tuple(3, 3, 8));
}

TEST(CollatzFixture, eval7)
{
    ASSERT_EQ(collatz_eval(make_pair(300, 500)), make_tuple(300, 500, 144));
}

TEST(CollatzFixture, eval8)
{
    ASSERT_EQ(collatz_eval(make_pair(1000, 2000)), make_tuple(1000, 2000, 182));
}

TEST(CollatzFixture, eval9)
{
    ASSERT_EQ(collatz_eval(make_pair(100000, 50000)), make_tuple(100000, 50000, 351));
}

TEST(CollatzFixture, One_mmmMillion)
{
    ASSERT_EQ(collatz_eval(make_pair(1, 1000000)), make_tuple(1, 1000000, 525));
}

TEST(CollatzFixture, negative)
{
    ASSERT_EQ(collatz_eval(make_pair(-1, 100)), make_tuple(-1, 100, -1));
}

TEST(CollatzFixture, negative2)
{
    ASSERT_EQ(collatz_eval(make_pair(1, -100)), make_tuple(1, -100, -1));
}

TEST(CollatzFixture, negative3)
{
    ASSERT_EQ(collatz_eval(make_pair(-1, -100)), make_tuple(-1, -100, -1));
}

TEST(CollatzFixture, zero)
{
    ASSERT_EQ(collatz_eval(make_pair(0, 0)), make_tuple(0, 0, -1));
}

TEST(CollatzFixture, zero2)
{
    ASSERT_EQ(collatz_eval(make_pair(0, 10)), make_tuple(0, 10, -1));
}

TEST(CollatzFixture, zero3)
{
    ASSERT_EQ(collatz_eval(make_pair(10, 0)), make_tuple(10, 0, -1));
}

TEST(CollatzFixture, oneOne)
{
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
