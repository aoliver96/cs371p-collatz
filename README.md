# CS371p: Object-Oriented Programming Collatz Repo

* Name: Alex Oliver

* EID: AJO695

* GitLab ID: aoliver96

* HackerRank ID: oliver_alex96

* Git SHA: ccee1a3a27d77df2fa7b663ee169adb731180aa1

* GitLab Pipelines: https://gitlab.com/aoliver96/cs371p-collatz/-/pipelines

* Estimated completion time: 20 hours

* Actual completion time: (actual time in hours, int or float) Approximately 16 hours

* Comments: (any additional comments you have)
